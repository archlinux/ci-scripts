==========
CI scripts
==========

This repository contains scripts and gitlab CI files for special build
environments, that can be used in other projects.

All available scripts are located in the `scripts <scripts/>`_ directory.

All Gitlab CI files are located top-level in the repository.

License
=======

The files in this repository are licensed under the terms of the
**GPL-3.0-or-later** (see `LICENSE <LICENSE>`_).
